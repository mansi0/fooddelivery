-- insert into customer values(1,'Mansi Mandhare','sr.no 7/1/1 sani super','shinde nagar','tanushree hospital','pune','maharashtra','mansimandhare.mm@gmail.com','7083988679','12345','28 nov 2019','false');

insert into hotel values(
    '101','man','mansimandhare.mm@gmail.com','7089483829','vaishali','lane no 4','fc road',
    'near fc college','pune','maharashtra','10:0:0','21:0:0','23456','2000-12-23',
    't','open','veg',array['1','0','0','1','0','0','0','1','0','1'],
    array['1','0','0','0','1','0','0','1'],'1.jpg','true');

insert into hotel values(
    '102','man','mansimandhare2000@gmail.com','7089483829','rupali','lane no 7','fc road',
    'near fc college','pune','maharashtra','11:0:0','22:0:0','2056','2005-09-01',
    't','open','veg',array['1','1','1','1','1','1','1','1','0','1'],
    array['1','0','0','0','1','0','0','1'],'2.jpg','true');

insert into hotel values(
    '103','man','a@gmail.com','70894565829','wadeshwar','khaugalli','fc road',
    'near goodluck chowk','pune','maharashtra','10:0:0','19:0:0','1500','2005-07-05',
    't','open','nonveg',array['1','1','1','1','1','1','1','1','0','1'],
    array['1','1','1','0','1','1','0','1'],'3.jpg','true');

insert into hotel values(
    '104','man','b@gmail.com','9889483829','samrat','building no 4','wakdewadi',
    'near ganapati mandir','pune','maharashtra','10:0:0','20:0:0','2000','2009-02-01',
    't','open','veg',array['1','1','0','1','1','0','1','1','0','1'],
    array['1','0','1','0','1','1','1','1'],'4.jpg','true');

insert into hotel values(
    '105','man','c@gmail.com','7083453829','lalit mahal','lane no 10,hirkani society','shivajinagar',
    'near dargah','pune','maharashtra','9:0:0','22:0:0','1000','2007-03-10',
    't','open','veg',array['0','1','1','1','0','1','1','1','0','1'],
    array['1','1','1','0','1','0','1','1'],'5.jpg','true');

insert into hotel values(
    '106','man','d@gmail.com','7083450099','durvankur','lane no 1','sadashiv peth',
    'near dnyanprabodhini','pune','maharashtra','11:0:0','19:0:0','1050','2007-02-11',
    't','open','veg',array['1','1','1','1','0','1','1','1','0','1'],
    array['1','0','1','0','1','1','1','1'],'6.jpg','true');

insert into hotel values(
    '107','man','e@gmail.com','9083453829','gandharva','lane no 4,ramraj society','shivajinagar',
    'near petrolpump','pune','maharashtra','10:0:0','21:0:0','900','2010-04-15',
    't','open','both',array['1','1','0','1','0','1','1','1','1','1'],
    array['0','1','1','1','1','0','1','1'],'7.jpg','true');

insert into hotel values(
    '108','man','f@gmail.com','9081153829','kokpa','2nd floor,shrinivas complex','wakdewadi',
    'near sakhar sankul','pune','maharashtra','11:0:0','21:0:0','1900','2011-03-20',
    't','open','nonveg',array['1','1','1','1','0','1','1','0','1','1'],
    array['1','1','1','0','1','1','1','1'],'8.jpg','true');

insert into hotel values(
    '109','man','g@gmail.com','9082901384','green signal','floor 2,redx mall','wakdewadi',
    'near gas agency','pune','maharashtra','10:0:0','21:0:0','1900','2014-04-15',
    't','open','both',array['1','1','1','1','0','1','0','1','1','0'],
    array['1','1','1','0','1','0','1','0'],'9.jpg','true');

insert into hotel values(
    '110','man','h@gmail.com','8883453829','the vegetable','lane no 9','sadashivpeth',
    'near modern college','pune','maharashtra','10:0:0','21:0:0','900','2010-04-15',
    'f','open','nonveg',array['1','0','1','1','0','1','1','1','1','1'],
    array['1','1','1','1','1','1','1','1'],'10.jpg','true');

insert into hotel values(
    '111','man','i@gmail.com','8884553829','bbq','flr no2','shaniwarpeth',
    'near petrolpump','pune','maharashtra','09:0:0','22:0:0','1900','2013-12-23',
    'f','open','both',array['1','1','1','1','0','0','1','1','0','1'],
    array['1','1','0','1','1','0','1','1'],'11.jpg','true');

insert into hotel values(
    '112','man','j@gmail.com','9884553829','joshi wada','lane no 11','shaniwarpeth',
    'near balgandharva','pune','maharashtra','08:0:0','23:0:0','500','2014-04-11',
    'f','open','veg',array['1','0','1','1','0','1','1','1','1','1'],
    array['1','1','0','1','1','0','0','0'],'12.jpg','true');

insert into hotel values(
    '113','man','k@gmail.com','7984553829','Ranvara','lane no 7','sadashivpeth',
    'near hdfcbank','pune','maharashtra','11:0:0','21:0:0','900','2019-11-09',
    'f','open','nonveg',array['1','0','1','1','0','0','1','1','0','0'],
    array['1','1','0','1','0','1','1','0'],'13.jpg','true');

insert into hotel values(
    '114','man','l@gmail.com','7784553829','panchali','flr no1','somvarpeth',
    'near centralmall','pune','maharashtra','09:0:0','23:0:0','1500','2020-12-13',
    'f','open','both',array['0','1','1','0','0','0','1','1','0','1'],
    array['1','1','0','0','1','0','1','0'],'14.jpg','true');

insert into hotel values(
    '115','man','m@gmail.com','9884773829','visawa','flr no5','somvarpeth',
    'near modernclg','pune','maharashtra','10:0:0','20:0:0','1200','2017-10-05',
    'f','open','both',array['1','0','1','1','1','1','1','1','0','1'],
    array['1','1','0','1','1','1','1','0'],'15.jpg','true');

insert into hotel values(
    '116','man','n@gmail.com','9084553829','mayur','flr no2','jm road',
    'near maharastrabank','pune','maharashtra','10:0:0','22:0:0','1400','2015-07-07',
    'f','open','veg',array['0','0','1','1','0','0','1','1','0','1'],
    array['1','1','0','1','1','0','0','0'],'16.jpg','true');



/*food*/
insert into food values('1001','pohe','1','Light');
insert into food values('1002','upma','1','Light');
insert into food values('1003','misal','1','Heavy');
insert into food values('1004','idli','1','Heavy');
insert into food values('1005','dosa','1','Light');

insert into food values('1006','manchow soup','2','Soup');
insert into food values('1007','manchurian','2','Chinese');
insert into food values('1008','paneer 65','2','Punjabi');
insert into food values('1009','paneer chilli dry','2','Punjabi');
insert into food values('1010','corn Soup','2','Soup');
insert into food values('1011','V=veg lollipop','2','Chinese');
insert into food values('1012','spring roll','2','Chinese');

insert into food values('1013','roti','3','Roti');
insert into food values('1014','simple rice','3','Rice');
insert into food values('1015','butter roti','3','Roti');
insert into food values('1016','jeera rice','3','Rice');
insert into food values('1017','paneer tikka','3','Gravy');
insert into food values('1018','kaju masala','3','Gravy');
insert into food values('1019','veg biryani','3','Rice');
insert into food values('1020','chicken biryani','3','Rice');
insert into food values('1021','mutton biryani','3','Rice');
insert into food values('1022','paneer masala','3','Gravy');
insert into food values('1023','veg kolhapuri','3','Gravy');
insert into food values('1024','veg maratha','3','Gravy');
insert into food values('1025','kaju curry','3','Gravy');

insert into food values('1026','gulam jamun','4','Sweets');
insert into food values('1027','chocolate pastry','4','Cake');
insert into food values('1028','carrot halwa','4','Sweets');
insert into food values('1029','chocolate balls','4','Cake');
insert into food values('1030','rasgulla','4','Sweets');

insert into food values('1031','panner tandoori pizza','5','Pizza');
insert into food values('1032','paneer makhni pizza','5','Pizza');
insert into food values('1033','aloo tikki burger','5','Burger');
insert into food values('1034','veggie burger','5','Burger');
insert into food values('1035','salty fries','5','Fries');
insert into food values('1036','cheese fries','5','Fries');
insert into food values('1037','super nachos','5','Nachos');
insert into food values('1038','loaded veggie nachos','5','Nachos');
insert into food values('1039','white pasta ','5','Pasta');
insert into food values('1040','masala pasta','5','Pasta');
insert into food values('1041','grill sandwitch','5','Sandwitch');
insert into food values('1042','veg sandwitch','5','Sandwitch');
insert into food values('1043','veg momos','5','Momos');
insert into food values('1044','chicken momos','5','Momos');

insert into food values('1045','vin de pays','6','Wine');
insert into food values('1046','dubble','6','Beer');
insert into food values('1047','martini','6','Vodka');
insert into food values('1048','burgundy wine','6','Wine');
insert into food values('1049','brown ale','6','Beer');

insert into food values('1050','hot cappuccino','7','Hot');
insert into food values('1051','iced cappuccino','7','Cold');

insert into food values('1052','mango juice','8','Fruit Juice');
insert into food values('1053','lemon soda','8','Aerated Juice');
insert into food values('1054','water melon juice','8','Fruit Juice');
insert into food values('1055','chickoo juice','8','Fruit Juice');
insert into food values('1056','fresh fruit juice','8','Aerated Juice');

insert into food values('1057','blue mojito','9','Mojito');
insert into food values('1058','monin mojito mint syrup','9','Mojito');
insert into food values('1059','princess nojito','9','Nojito');
insert into food values('1060','sainsbury nojito','9','Nojito');

insert into food values('1061','chocolate cornetto','10','Cone');
insert into food values('1062','butter scotch','10','Cone');
insert into food values('1063','vanilla cups','10','Cup');
insert into food values('1064','pista cups','10','Cup');
insert into food values('1065','chocolate chips','10','Family Pack');
insert into food values('1066','cream and cookies','10','Family Pack');
insert into food values('1067','magnum','10','Candy');
insert into food values('1068','chocobar','10','Candy');
insert into food values('1069','malai kulfi','10','Kulfi');
insert into food values('1070','mix kulfi','10','Kulfi');

/*hotel_food*/
insert into hotel_food values('2000','101','1001','Garnished with cheese and coriander','',50);
insert into hotel_food values('2001','101','1050','Extra Chcocolate','50','');
insert into hotel_food values('2002','102','1002','Garnished with cheese and coriander',50,'');
insert into hotel_food values('2003','102','1004','Garnished with cheese and coriander','60','');
insert into hotel_food values('2004','103','1003','Garnished with cheese and coriander','60','');
insert into hotel_food values('2005','103','1005','Garnished with cheese and coriander','50','');
insert into hotel_food values('2006','104','1006','Garnished with cheese and coriander','50','');
insert into hotel_food values('2007','104','1007','Garnished with cheese and coriander','100','');
insert into hotel_food values('2008','105','1008','Garnished with cheese and coriander','80','');
insert into hotel_food values('2009','105','1009','Garnished with cheese and coriander','100','');
insert into hotel_food values('2010','106','1010','Garnished with cheese and coriander','90','');
insert into hotel_food values('2011','106','1011','Garnished with cheese and coriander','110','');
insert into hotel_food values('2012','107','1012','Garnished with cheese and coriander','80','');
insert into hotel_food values('2013','107','1013','Garnished with cheese and coriander','90','');
insert into hotel_food values('2014','108','1014','Garnished with cheese and coriander','50','');
insert into hotel_food values('2015','108','1015','Garnished with cheese and coriander','99','');
insert into hotel_food values('2016','109','1016','Garnished with cheese and coriander','100','');
insert into hotel_food values('2017','109','1017','Garnished with cheese and coriander','120','');
insert into hotel_food values('2018','110','1018','Garnished with cheese and coriander','130','');
insert into hotel_food values('2019','111','1019','Garnished with cheese and coriander','150','');
insert into hotel_food values('2020','112','1020','Garnished with cheese and coriander','145','');
insert into hotel_food values('2021','113','1021','Garnished with cheese and coriander','145','');
insert into hotel_food values('2022','114','1022','Garnished with cheese and coriander','140','');
insert into hotel_food values('2023','115','1023','Garnished with cheese and coriander','140','');
insert into hotel_food values('2024','116','1024','Garnished with cheese and coriander','140','');
insert into hotel_food values('2025','110','1025','Garnished with cheese and coriander','90','');
insert into hotel_food values('2026','111','1026','Garnished with cheese and coriander','70','');
insert into hotel_food values('2027','112','1027','Garnished with cheese and coriander','80','');
insert into hotel_food values('2028','113','1028','Garnished with cheese and coriander','50','');
insert into hotel_food values('2029','114','1029','Garnished with cheese and coriander','45','');
insert into hotel_food values('2030','115','1030','Garnished with cheese and coriander','45','');
insert into hotel_food values('2031','116','1031','Garnished with cheese and sauce','100','medium');
insert into hotel_food values('2032','101','1032','Garnished with cheese and sauce','150','large');
insert into hotel_food values('2033','102','1033','Garnished with cheese and sauce','60','');
insert into hotel_food values('2034','103','1034','Garnished with cheese and sauce','70','');
insert into hotel_food values('2035','104','1035','Garnished with cheese and sauce','45','');
insert into hotel_food values('2036','105','1036','Garnished with cheese and sauce','45','');
insert into hotel_food values('2037','106','1037','Garnished with cheese and sauce','50','');
insert into hotel_food values('2038','107','1038','Garnished with cheese and sauce','60','');
insert into hotel_food values('2039','108','1039','Garnished with cheese and sauce','45','');
insert into hotel_food values('2040','109','1040','Garnished with cheese and sauce','65','');
insert into hotel_food values('2041','110','1041','Garnished with cheese and coriander','55','');
insert into hotel_food values('2042','111','1042','Garnished with cheese and coriander','50','');
insert into hotel_food values('2043','112','1043','Garnished with cheese and coriander','70','');
insert into hotel_food values('2044','113','1044','Garnished with cheese and coriander','80','');
insert into hotel_food values('2045','114','1045','Extra 10% Drink','200','');
insert into hotel_food values('2046','115','1046','Extra 10% Drink','150','');
insert into hotel_food values('2047','116','1047','Extra 10% Drink','200','');
insert into hotel_food values('2048','101','1048','Extra 10% Drink','150','');
insert into hotel_food values('2049','102','1049','Extra 10% Drink','200','');
insert into hotel_food values('2050','103','1050','Extra 10%','45','');
insert into hotel_food values('2051','104','1051','Extra 10%','45','');
insert into hotel_food values('2052','105','1052','Extra 10%','30','');
insert into hotel_food values('2053','106','1053','Extra 10%','30','');
insert into hotel_food values('2054','107','1054','Extra 10%','40','');
insert into hotel_food values('2055','108','1055','Extra 10%','90','');
insert into hotel_food values('2056','109','1056','Extra 10%','90','');
insert into hotel_food values('2057','110','1057','Extra 10%','80','');
insert into hotel_food values('2058','111','1058','Extra 10%','80','');
insert into hotel_food values('2059','112','1059','Extra 10%','75','');
insert into hotel_food values('2060','113','1060','Extra 10%','70','');
insert into hotel_food values('2061','114','1061','Extra 10%','80','');
insert into hotel_food values('2062','115','1062','Extra 10%','70','');
insert into hotel_food values('2063','116','1063','Extra 10%','90','');
insert into hotel_food values('2064','101','1064','Extra 10%','50','');
insert into hotel_food values('2065','102','1065','Extra 10%','100','');
insert into hotel_food values('2066','103','1066','Extra 10%','100','');
insert into hotel_food values('2067','104','1067','Extra 10%','70','');
insert into hotel_food values('2068','105','1068','Extra 10%','70','');
insert into hotel_food values('2069','106','1069','Extra 10%','80','');
insert into hotel_food values('2070','107','1070','Extra 10%','80','');
insert into hotel_food values('2071','108','1001','Extra 10%','90','');
insert into hotel_food values('2072','109','1020','Extra 10%','100','');
insert into hotel_food values('2073','110','1014','Extra 10%','90','');
insert into hotel_food values('2074','111','1015','Extra 10%','70','');
insert into hotel_food values('2075','112','1010','Extra 10%','90','');
insert into hotel_food values('2076','113','1050','Extra 10%','100','');
insert into hotel_food values('2077','114','1030','Extra 10%','80','');
insert into hotel_food values('2078','115','1034','Extra 10%','90','');
insert into hotel_food values('2079','116','1025','Extra 10%','70','');
insert into hotel_food values('2080','101','1026','Extra 10%','50','');
insert into hotel_food values('2081','102','1022','Extra 10%','70','');
insert into hotel_food values('2082','103','1027','Extra 10%','50','');
insert into hotel_food values('2083','104','1021','Extra 10%','60','');
insert into hotel_food values('2084','105','1055','Extra 10%','50','');
insert into hotel_food values('2085','106','1052','Extra 10%','40','');
insert into hotel_food values('2086','107','1051','Extra 10%','50','');
insert into hotel_food values('2087','108','1061','Extra 10%','90','');
insert into hotel_food values('2088','109','1062','Extra 10%','100','');
insert into hotel_food values('2089','110','1063','Extra 10%','90','');
insert into hotel_food values('2090','111','1064','Extra 10%','90','');
insert into hotel_food values('2091','112','1051','Extra 10%','90','');
insert into hotel_food values('2092','113','1031','Extra 10% cheese','70','small');
insert into hotel_food values('2093','114','1032','Extra 10% cheese','150','large');
insert into hotel_food values('2094','115','1033','Extra 10% cheese','100','');
insert into hotel_food values('2095','116','1034','Extra 10% cheese','100','');
insert into hotel_food values('2096','101','1035','Extra 10% cheese','100','');
insert into hotel_food values('2097','102','1036','Extra 10% cheese','100','');
insert into hotel_food values('2098','103','1037','Extra 10% cheese','150','');
insert into hotel_food values('2099','104','1038','Extra 10% cheese','100','');
insert into hotel_food values('2100','105','1039','Extra 10% cheese','100','');
insert into hotel_food values('2101','101','1002','Garnished with cheese and coriander',50,'');








