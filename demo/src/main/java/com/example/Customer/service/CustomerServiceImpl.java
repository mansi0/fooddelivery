package com.example.Customer.service;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import com.example.Customer.dao.CustomerDao;
import com.example.Customer.entity.CustomerEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import java.lang.Object.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
//import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;;

/**
 * CustomerServiceImpl
 */
@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private JavaMailSender JavaMailSender;
  @Resource
  CustomerDao customerDao;

  Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
  private ExecutorService executor = Executors.newSingleThreadExecutor();

  @Override
  public List<CustomerEntity> getDetail() {

    List<CustomerEntity> listOfCustomer = customerDao.getDetail();
    return listOfCustomer;
  }

  @Override
  public List<CustomerEntity> getDetailsByEmailId(CustomerEntity customerEntity) {
    String emailId=customerEntity.getEmailId();
    List<CustomerEntity> listOfCustomer = customerDao.getDetailsByEmailId(emailId);
    return listOfCustomer;
  
  }

  @Override
  public List<CustomerEntity> getDetailsByCustomerId(String customerId) {
    List<CustomerEntity> listOfCustomer = customerDao.getDetailsByCustomerId(customerId);
    //System.out.println("in service ####"+listOfCustomer);
    return listOfCustomer;
  
  }

  public int checkDuplicationOfEmail(CustomerEntity customerEntity) {

    try {
      // String email = user.getEmail();
      List<CustomerEntity> listOfCustomer = customerDao.checkDuplicationOfEmail(customerEntity);
      if (listOfCustomer.size() > 0) {

        System.out.println("this email already exist:: " + customerEntity.getEmailId());
        return 1;
        // return ResponseEntity.status(HttpStatus.OK).body("Customer Already Exits");
      }
    } catch (Exception e) {

      logger.error("SERVICE::CustomerServiceImpl::addCustomer::checkDuplicationForEmail::error:: " + e.getMessage());
      return 0;
    }
    return 0;

  }


  public int addCustomer(CustomerEntity customerEntity) {
    
        
        int resultOfDuplication = checkDuplicationOfEmail(customerEntity);
    
        if(resultOfDuplication == 0) {
          String password = customerEntity.getPassword();
        
          try {
          
            String encryptedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
            logger.debug("SERVICE::CustomerServiceImp::addCustomer::encryptedPassword:: " + encryptedPassword);
          
            customerEntity.setPassword(encryptedPassword);
            int addCustomerResponse = customerDao.addCustomer(customerEntity);
    
            if(addCustomerResponse > 0) {
              Future<Integer> response = sendGreetingEmail(customerEntity.getName(), customerEntity.getEmailId());
             logger.debug("SERVICE::UserServiceImp::addUser::response:: " + addCustomerResponse); 
            }
            else {
    
            logger.debug("SERVICE::CustomerServiceImp::addCustomer::Customer not get added");
             
            }
    
          return 1;
        }
        catch(Exception e) {
    
          logger.error("SERVICE::CustomerServiceImp::addCustomer::error:: " + e.getMessage());
          return 0;
        }
       }
        else 
          return -1;
      }

  public Future<Integer> sendGreetingEmail(String name, String email) {
   
        try {
          return executor.submit(() -> {
    
            String message = String.format("<html>Hi <b>%s</b>,", name);
            message += "<br><br>&nbsp;&nbsp;Welcome on Taste On Way Service !!!Our team just wanted to say hello and make sure everything's in place for you to feel at home on \"Tast On Way\". ";
            message += "<br><br>&nbsp;&nbsp;          We are always here to serve your cravings,thank you for joining us. Your feedback and loyalty will go a long way to help us make you proud of us.";
            message += "<br><br><br> Cheers!, <br> Taste On Way Team.</html>";
            
            logger.debug("SERVICE::UserServiceImp::sendGreetingEmail::message:: " + message);
    
            MimeMessage mimeMessage = JavaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
            
            helper.setTo(email);
            helper.setSubject("Welcome in Foodie Team!!");     
            helper.setText(message, true);
    
            try {
              JavaMailSender.send(mimeMessage);	  
            } catch (Exception e) {
              System.out.println(">>>>>>>>>>>>> ERRROR " +e);
              return 0;
            }
            
            logger.debug("SERVICE::UserServiceImp::sendGreetingEmail::sendMessage Successfully");
    
            // Fetch user by emailId
            List<CustomerEntity> listOfUser = customerDao.fetchByEmailId(email);
            CustomerEntity customerEntity = listOfUser.get(0);
    
            String customerUUID = customerEntity.getCustomerId();
    
            customerEntity.setNotification(true);
            customerEntity.setCustomerId(customerUUID);
            customerDao.updateCustomer(customerEntity);
    
            return 1;
          });
    
        } catch (Exception e) {
          
          logger.error("SERVICE::UserServiceImp::sendGreetingEmail::error:: " + e.getMessage());
          e.printStackTrace();
    
          return executor.submit(() -> {
            return 0;
          });
        }
      }


  @Override
  public int loginCustomer(String email, String psw) {

    try {

     List<CustomerEntity> listOfCustomer = customerDao.fetchByEmailId(email);
     
      if(listOfCustomer.size() == 0) 
         return -1;
      else if(listOfCustomer.size()>0) {

       CustomerEntity customerEntity=new CustomerEntity();
       customerEntity=listOfCustomer.get(0);

        BCryptPasswordEncoder bCrypt =new BCryptPasswordEncoder();
        boolean isPasswordMatches = bCrypt.matches(psw, customerEntity.getPassword());
        System.out.println("value :"+ isPasswordMatches);

       if(isPasswordMatches) {
            return 1;
       }
       else return 0;

    }
  }
  catch(Exception e) {
    return -2;
  }
  return -2;
}

}