package com.example.Deliveryboy.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.example.Deliveryboy.entity.DeliveryboyEntity;
import com.example.Deliveryboy.service.DeliveryboyService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * DeliveryboyController
 */
@RestController
@RequestMapping(path = "/deliveryboy")
public class DeliveryboyController {
    @Resource
    DeliveryboyService deliveryboyService;

    static final Logger logger = LoggerFactory.getLogger(DeliveryboyController.class);

    @GetMapping(value = "/details")
    public int getDetails() {

        List<DeliveryboyEntity> listOfDeliveryboy = deliveryboyService.getDetail();
        return listOfDeliveryboy.size();
    }

    // get details by emailid
    @PostMapping(value = "/getdeliveryboybyemailid", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DeliveryboyEntity> getDetailsByEmailId(@RequestBody String parameters)
            throws JsonMappingException, JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        DeliveryboyEntity deliveryboyEntity = mapper.readValue(parameters, DeliveryboyEntity.class);

        // System.out.println(parameters);
        // parameters=parameters+".com";
        // System.out.println(parameters);
        List<DeliveryboyEntity> listOfDeliveryboyEntities = deliveryboyService
                .getDetailsByEmailId(deliveryboyEntity.getDeliveryboyEmailId());
        return listOfDeliveryboyEntities;

    }

    // get details by deliveryboyid by post
    @GetMapping(value = "/getdeliveryboybydeliveryboyid/{parameters}")
    public List<DeliveryboyEntity> getDetailsByDeliveryboyId(@PathVariable String parameters)
            throws JsonParseException, JsonMappingException, IOException {
        // System.out.println(parameters);
        // parameters=parameters+".com";
        // System.out.println(parameters);

        List<DeliveryboyEntity> listOfDeliveryboyEntities = deliveryboyService.getDetailsByDeliveryboyId(parameters);
        return listOfDeliveryboyEntities;

    }
    // update by deliveryboystatus

    @PostMapping(value = "/updatedelieryboybystatus")
    public ResponseEntity<?> updateDeliveryboy(@RequestBody String parameters)
            throws JsonParseException, JsonMappingException, IOException {
        // logger.debug("POST:CustomerController:addCustomer::parameters::
        // "+parameters);
        ObjectMapper mapper = new ObjectMapper();
        DeliveryboyEntity deliveryboyEntity = mapper.readValue(parameters, DeliveryboyEntity.class);

        try {
            int result = deliveryboyService.updateOrderByStatus(deliveryboyEntity);
            if (result == 1)/* 200 */ {
                System.out.println("updated deliveryboy for status");
                return ResponseEntity.status(HttpStatus.OK).body("deliveryboy updated for status ");
            } else if (result == 0)// 500
            {
                System.out.println("delivery boy is not updated for status");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error");
            }
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("internal server error");
        }
        return null;
    }


    // update by deliveryboyactivity

    @PostMapping(value = "/updatedelieryboybyactivity")
    public ResponseEntity<?> updateDeliveryboyByActivity(@RequestBody String parameters)
            throws JsonParseException, JsonMappingException, IOException {
        // logger.debug("POST:CustomerController:addCustomer::parameters::
        // "+parameters);
        ObjectMapper mapper = new ObjectMapper();
        DeliveryboyEntity deliveryboyEntity = mapper.readValue(parameters, DeliveryboyEntity.class);

        try {
            int result = deliveryboyService.updateOrderByActivity(deliveryboyEntity);
            if (result == 1)/* 200 */ {
                System.out.println("updated deliveryboy for activity");
                return ResponseEntity.status(HttpStatus.OK).body("deliveryboy updated for activity ");
            } else if (result == 0)// 500
            {
                System.out.println("delivery boy is not updated for status");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error");
            }
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("internal server error");
        }
        return null;
    }

    //getdetails by activity=0 and status=1
    @GetMapping(value = "/getdetailsbyactivity")
    public List<DeliveryboyEntity> getdetailsbyactivity() throws ParseException {

        List<DeliveryboyEntity> deliveryboyEntities = deliveryboyService.getDetailsByActivity();
        return deliveryboyEntities;
    }


    // adddeliveryboy
    @PostMapping(value = "/adddeliveryboy", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addDeliveryboy(@RequestBody String parameters)
            throws JsonParseException, JsonMappingException, IOException {
        // logger.debug("POST:DeliveryboyController:addDeliveryboy::parameters::
        // "+parameters);
        ObjectMapper mapper = new ObjectMapper();
        DeliveryboyEntity deliveryboyEntity = mapper.readValue(parameters, DeliveryboyEntity.class);
        // System.out.println("delivery boy in controller ::" + deliveryboyEntity);

        try {
            int result = deliveryboyService.addDeliveryboy(deliveryboyEntity);
            if (result == -1)// 400
            {
                Map<String, Object> body = new HashMap<String, Object>();

                body.put("message", "DeliveryBoy already exist already exist");
                body.put("status", HttpStatus.BAD_REQUEST.value());

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
            } else if (result == 0)// 500
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Exception occur");
            else if (result == 1)// 200
                return ResponseEntity.status(HttpStatus.OK).body("Deliveryboy added successfully ");

        } catch (Exception e) {
            logger.error("POST:DeliveryboyController:addDeliveryboy::error:: " + e.getStackTrace());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("error occured");
        }
        return null;
    }

    @PostMapping(value = "/logindeliveryboy", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> loginCustomer(@RequestBody String parameters)
            throws JsonParseException, JsonMappingException, IOException {
        // logger.debug("POST:CustomerController:addCustomer::parameters::
        // "+parameters);
        ObjectMapper mapper = new ObjectMapper();
        DeliveryboyEntity customerEntity = mapper.readValue(parameters, DeliveryboyEntity.class);
        String email = customerEntity.getDeliveryboyEmailId();
        String psw = customerEntity.getDeliveryboyPassword();
        // "+parameters);
        try {
            System.out.println(email + "," + psw + "cccccccc");
            int result = deliveryboyService.loginDeliveryboy(email, psw);
            if (result == -1)// 404
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Deliveryboy Not Found");
            if (result == 0)// 400
            {
                Map<String, Object> body = new HashMap<String, Object>();

                body.put("message", "Invalid Password");
                body.put("status", HttpStatus.BAD_REQUEST.value());

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
            }

            if (result == 1)// 200
                return ResponseEntity.status(HttpStatus.OK).body("Valid Entry");
            if (result == -2)// 500
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Exception occur");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Exception occur");
        }
        return null;

    }
}
