package com.example.Imageloading.entity;

/**
 *ImageEntity 
 */
public class ImageEntity {

    String imagePath;

    public String getImage() {
        return imagePath;
    }

    public void setImage(String imagePath) {
        this.imagePath = imagePath;
    }
}